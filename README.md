Make Docker
===========

Build docker images with make so that images are only rebuilt when the
`Dockerfile` or other dependencies change.

Usage
-----

Build or rebuild the docker image, only if `Dockerfile` or a `Dockerfile`
prerequisite have changed.

```Shell
make
```

Remove the docker image if it exists, and ensure the image will be built
the next time you make.

```Shell
make clean
```

Discussion
----------

Ideally, we would have a target that would build the image when `Dockerfile`
changes by making `Dockerfile` a prerequisite.

```Shell
all: Dockerfile
	docker build -t image-tag .
```

However, this doesn't work because nothing creates a file `all`, so this
target is always out of date, and will always run. We would be in a similar
situation if `all` were a phony target.

We could try making a `Dockerfile` target.

```Shell
all: Dockerfile

Dockerfile:
	docker build -t image-tag .
```

The problem here is that `Dockerfile` target has no prerequisites, so it
is always up to date, and will never run. This is because a `Dockerfile`
is itself a "makefile" for docker. A make target is usually an output,
e.g., the result of a compilation. However a `Dockerfile` is an input that
describes how to create the output, an image. But we can't readily make the
image a target, so we have to try to make the `Dockerfile` a prerequisite
of some target that will build the image.

In order to make a target build an image when `Dockerfile` is out of date, we
first make sure that the target creates its target file, so it isn't always
out of date. Then we make `Dockerfile` a prerequisite of that target. Now
the targe will only run and build the image when `Dockerfile` changes.

```
all: Dockerfile
	docker build -t image-tag .
	touch $@
```

But this is a little inconvenient because now we have an `all` file
hanging around. It would be nicer if this file were at least in a hidden
directory. So let's use a proxy target. `Dockerfile` will be a prerequisite of
the proxy target, and the proxy target will create its target file in a hidden
directory. Now we can make `all` a phony target and give it the proxy target
as a prerequisite, which effectively makes `Dockerfile` a prerequiste of `all`.

```Shell
.PHONY: all

all: .make/image

.make/image: Dockerfile
	docker build -t make-test .
	touch $@
```

We can also create a `Dockerfile` target with further prerequisites for
the image. If any of the `Dockerfile` prerequisites are out of date, the
`Dockerfile` target will touch `Dockerfile`, which will cause the proxy target
to be out of date, and so on up the chain, so the image will be rebuilt if
`Dockerfile` or any of its prerequisites change.

```Shell
Dockerfile: script1 script2 script3
	touch $@
```

