.PHONY: all clean

all: .make/image

.make/image: Dockerfile | .make
	docker build -t make-test .
	touch $@

Dockerfile: start
	touch $@

.make:
	mkdir $@

clean:
ifeq ($(shell docker image ls make-test | wc -l), 2)
	docker rmi make-test
endif
	$(RM) .make/image

